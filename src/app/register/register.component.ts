import { Component, OnInit } from '@angular/core';
import { GeralService } from '../services/geral.service';
import { UtilService } from '../services/util.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
  public backgroundImage = './assets/imgs/bg_cars.png';
  public email;
  public name;
  public password;
  public conf_password;  

  constructor( public svc : GeralService,
    public util : UtilService,
    public route : Router) { }

  ngOnInit() {
  }

  register(){      
    if (this.password == this.conf_password){
      this.util.startLoad();
      this.svc.doRegister({email:this.email, password: this.password, name: this.name}).subscribe(res=>{        
        this.util.stopLoad();
        this.util.msgSuccess('Sucesso!','Cadastro realizado, efetue o login');        
        this.route.navigate[('LoginPage')];
      }, err=>{        
        this.util.stopLoad();
        this.util.msgError('Ops.','E-mail cadastrado já existe!');
      })

    } else {
      this.util.msgError('Ops!','As senhas informadas não conferem.');      
    }
  }

  back(){
    this.route.navigate(['login']);
  }
}

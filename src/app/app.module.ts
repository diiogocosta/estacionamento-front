import { AuthGuard } from './guards/auth-guard';
import { AppRoutes } from './app.routes';
import { UtilService } from './services/util.service';
import { GeralService } from './services/geral.service';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { HttpModule } from '@angular/http';
import { BlockUIModule } from 'ng-block-ui';
import { CookieService } from 'ngx-cookie-service';

import {
  ToastrModule,
  ToastNoAnimation,
  ToastNoAnimationModule,
} from 'ngx-toastr';

import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { AdminComponent } from './admin/admin.component';
import { AdminModule } from './admin/admin.module';
import { FormsModule } from '@angular/forms';
import { RegisterComponent } from './register/register.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegisterComponent,    
  ],
  imports: [
    BrowserModule,
    HttpModule,
    AppRoutes,
    AdminModule,
    FormsModule,
    BlockUIModule.forRoot(),
    NgbModule.forRoot(),
    ToastNoAnimationModule,
    ToastrModule.forRoot({
      toastComponent: ToastNoAnimation,
    })
  ],
  providers: [GeralService,UtilService, AuthGuard, CookieService],
  bootstrap: [AppComponent]
})
export class AppModule { }

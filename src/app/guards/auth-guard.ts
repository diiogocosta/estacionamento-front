import { CookieService } from 'ngx-cookie-service';
import { Injectable } from '@angular/core';
import { CanActivate } from '@angular/router/src/interfaces';
import { ActivatedRouteSnapshot, RouterStateSnapshot,Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { GeralService } from '../services/geral.service';

@Injectable()
export class AuthGuard implements CanActivate {
  private simpleUserRoutes : Array<any>;
  private funcRoutes : Array<any>;
  private adminRoutes : Array<any>;

  constructor(private cookie : CookieService, private route: Router, private svc : GeralService) { 
    this.simpleUserRoutes = ['/login','/register','/admin/pesquisa','/pesquisa'];
    this.funcRoutes = ['/admin/entradas','/admin/patio','/patio','/entradas'].concat(this.simpleUserRoutes);
    this.adminRoutes = ['/admin/valores','/admin/relatorios','/admin/funcionarios'].concat(this.funcRoutes);
  }
    

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ) : Observable<boolean> | boolean {        
    if (this.cookie.check('token')){       
      this.svc.role = this.cookie.get('role');      
      
      if (this.svc.role == 1) 
        return true;                  
        
      if (this.svc.role == 2){
        console.log(state.url);
        if (this.funcRoutes.indexOf(state.url) < 0){
          this.route.navigate(['/admin/entradas']);
          return false            
        } else
          return true;
        }
        
      if (this.svc.role == 3 || this.svc.role == 4){
        console.log('passando aqui');
        console.log(this.simpleUserRoutes);
        console.log(state.url);
        console.log(this.simpleUserRoutes.indexOf(state.url));
        if (this.simpleUserRoutes.indexOf(state.url) < 0){
          this.route.navigate(['/admin/pesquisa']);
          return false
        } else
          return true;
      }
    } else {      
      this.route.navigate(['/login']);
      return false;
    }
  }

}

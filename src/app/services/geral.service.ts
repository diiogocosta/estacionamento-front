import { UtilService } from './util.service';
import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { Routes, Router } from '@angular/router';
import { ErrorObservable } from 'rxjs/observable/ErrorObservable';
import { CookieService } from 'ngx-cookie-service';

@Injectable()
export class GeralService {
  public token;
  private API_URL : string = "http://homologacao.degrahl.com.br/public/api/";     
  private headers;
  public role;
  public email;

  constructor(private http : Http, 
              private util : UtilService, 
              private route : Router,
              private cookie : CookieService) { }

  private refreshToken(){    
    if (this.cookie.check('token')){
      this.token = this.cookie.get('token');
      this.role = this.cookie.get('role');
      this.email = this.cookie.get('email');      
    }
    this.headers = new Headers({
      "Content-Type":"application/json",
      "Authorization":"Bearer "+this.token      
    });    
  }

  doLogout(){
    this.token = undefined;
    this.cookie.deleteAll( '/');      
    this.cookie.deleteAll( '../');   
    this.cookie.deleteAll( '../../');   
    this.route.navigate(['login']);
  }

  novoToken(){
    return this.http.post(this.API_URL+'login',{email:this.cookie.get('email'),password:this.cookie.get('password')},{headers:this.headers});
  }

  doLogin(data){              
    this.refreshToken();
    this.util.startLoad();
    this.http.post(this.API_URL+'login',data,{headers:this.headers}).subscribe(res =>{
      let content = JSON.parse((<any>res)._body);
      this.cookie.set('password', data.password );            
      this.cookie.set('token', content.access_token);
      this.token = content.access_token;
      
      
      if (!content.access_token)
        this.util.msgError('Ops','Email ou senha incorretos!')
      else {
        this.refreshToken();
        this.http.get(this.API_URL+'users/user',{headers:this.headers}).subscribe(res=>{
          let user = JSON.parse((<any>res)._body);               
          this.email = user.email;
          this.role = user.role;
          this.cookie.set('email', data.email );
          this.cookie.set('role', this.role );          
          this.util.stopLoad();
          this.route.navigate(['admin/entradas']);
        });        
      }
    },err => {
      this.util.stopLoad();
      this.util.msgError('Ops','Email ou senha incorretos!')
    })      
  }

  doRegister(data){
    this.refreshToken();
    return this.http.post(this.API_URL+'users/add',data,{headers:this.headers});
  } 

  doUpdateUser(data){
    this.refreshToken();
    return this.http.post(this.API_URL+'users/update',data,{headers:this.headers});
  }

  getListUsers(){
    this.refreshToken();
    return this.http.get(this.API_URL+'users/listAll',{headers:this.headers});
  }

  getListServicos(){
    this.refreshToken();    
    return this.http.get(this.API_URL+'additionalServices/listAll',{headers:this.headers});
  }

  doAddServicos(data){
    this.refreshToken();    
    return this.http.post(this.API_URL+'additionalServices/add',data,{headers:this.headers});
  }

  doUpdateServicos(data){
    this.refreshToken();    
    return this.http.post(this.API_URL+'additionalServices/update',data,{headers:this.headers});
  }  

  doDeleteServicos(data){
    this.refreshToken();    
    return this.http.post(this.API_URL+'additionalServices/delete',data,{headers:this.headers});
  }  

  getPrecos(){
    this.refreshToken();    
    return this.http.get(this.API_URL+'fees/listAll',{headers:this.headers});    
  }

  doUpdatePrecos(data){
    this.refreshToken();
    return this.http.post(this.API_URL+'fees/update',data,{headers:this.headers});
  }

  getPlacaSinesp(placa){        
    this.refreshToken();
    return this.http.post('http://gridsoft.com.br/sinesp/placa.php',{placa:placa},{headers:this.headers});
  }

  addVeiculo(data){
    this.refreshToken();
    return this.http.post(this.API_URL+'vehicles/add',data,{headers:this.headers});
  }

  updateVeiculo(data){
    this.refreshToken();
    return this.http.post(this.API_URL+'vehicles/update',data,{headers:this.headers});
  }

  getVeiculo(placa){
    this.refreshToken();
    if (placa == ''){
      this.util.msgInfo('Informação!','É obrigatório informar a placa');
      this.util.stopLoad();
    } else
      return this.http.get(this.API_URL+'vehicles/list?plate='+placa,{headers:this.headers});
  }

  listVeiculoParkingLot(placa){
    this.refreshToken();    
    return this.http.get(this.API_URL+'parkingLot/list?plate='+placa,{headers:this.headers});
  }

  doEntradaEstacionamento(placa){
    this.refreshToken();    
    return this.http.post(this.API_URL+'parkingLot/entrance',{plate:placa},{headers:this.headers});
  }

  patioListAll(params:string = ''){
    this.refreshToken();         

    if (params != '')
      return this.http.get(this.API_URL+'parkingLot/listAll'+params,{headers:this.headers})
    else
      return this.http.get(this.API_URL+'parkingLot/listAll?exited=false',{headers:this.headers});
  }  

  getPrice(id){
    this.refreshToken();
    return this.http.get(this.API_URL+'parkingLot/price?id='+id,{headers:this.headers});
  }

  doLiberaVeiculo(data){
    this.refreshToken();
    console.log(data);
    return this.http.post(this.API_URL+'parkingLot/release',data,{headers:this.headers});
  }

  doExitVeiculo(data){
    this.refreshToken();
    console.log(data);
    return this.http.post(this.API_URL+'parkingLot/exit',data,{headers:this.headers});
  }

  doAddService(data){
    this.refreshToken();
    return this.http.post(this.API_URL+'parkingLotAdditionalServices/addService',data,{headers:this.headers});
  }

  doPaymentAdditionalServices(data){
    this.refreshToken();
    return this.http.post(this.API_URL+'parkingLotAdditionalServices/paid',data,{headers:this.headers});
  }

  doPaymentHourly(data){
    this.refreshToken();
    return this.http.post(this.API_URL+'hourlyFees/paid',data,{headers:this.headers});
  }

  doPaymentMonth(data){
    this.refreshToken();
    return this.http.post(this.API_URL+'monthlyFees/paid',data,{headers:this.headers});
  }

}

import { Injectable } from '@angular/core';
import { BlockUI, NgBlockUI } from 'ng-block-ui';
import { ToastrService } from 'ngx-toastr';

@Injectable()
export class UtilService {
  @BlockUI() blockUI: NgBlockUI;

  constructor(private toastr: ToastrService) { }  

  startLoad(){
    this.blockUI.start('Carregando...');
  }

  stopLoad(){
    this.blockUI.stop();
  }

  msgInfo(title,msg){
    this.toastr.info(msg,title);
  }

  msgSuccess(title,msg){
    this.toastr.success(msg,title);
  }

  msgError(title,msg){
    this.toastr.error(msg,title);
  }

  dataAtualFormatada(){    
    var data = new Date();
    var dia = data.getDate();
    if (dia.toString().length == 1)
      dia = (<any>"0"+dia);
    var mes = data.getMonth()+1;
    if (mes.toString().length == 1)
      mes = (<any>"0"+mes);
    var ano = data.getFullYear();  
    var hora = data.getHours();
    var min = data.getMinutes();

    let minStr : string = min.toString();
    let hourStr : string = hora.toString();

    if (minStr.length == 1)
      minStr = '0'+minStr;
      
    if (hourStr.length == 1)
      hourStr = '0'+hourStr;      

    return dia+"/"+mes+"/"+ano+' '+hourStr+':'+minStr;  
  }





}

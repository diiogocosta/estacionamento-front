import { UtilService } from './../services/util.service';
import { GeralService } from './../services/geral.service';
import { Component, OnInit } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  public backgroundImage = './assets/imgs/bg_cars.png';
  public email;
  public password;  

  constructor(        
    public svc: GeralService,
    public util: UtilService,
    public cookie:CookieService,
    public route : Router
  ) { }

  ngOnInit() {    
    if (this.cookie.check('email')){            
      this.svc.doLogin({email:this.cookie.get('email'),password:this.cookie.get('password')});  
    }
  }

  login() {                
    this.svc.doLogin({email:this.email,password:this.password});          
  }

  goToSignup(){
    this.route.navigate(['register']);
  }
}

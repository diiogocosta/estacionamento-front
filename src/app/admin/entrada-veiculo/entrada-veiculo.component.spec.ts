import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EntradaVeiculoComponent } from './entrada-veiculo.component';

describe('EntradaVeiculoComponent', () => {
  let component: EntradaVeiculoComponent;
  let fixture: ComponentFixture<EntradaVeiculoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EntradaVeiculoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EntradaVeiculoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

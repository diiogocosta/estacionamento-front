import { CookieService } from 'ngx-cookie-service';
import { GeralService } from './../../services/geral.service';
import { UtilService } from './../../services/util.service';
import { Component, OnInit } from '@angular/core';
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-entrada-veiculo',
  templateUrl: './entrada-veiculo.component.html',
  styleUrls: ['./entrada-veiculo.component.scss']
})
export class EntradaVeiculoComponent implements OnInit {  
  public lista: any;
  public listaEfetiva = [];
  public dadosOK = false;
  public preencherManual = false;
  public placa = '';
  public dadosVeiculo;  
  public showServices = false;
  public parkingLot = undefined;
  public id_user = null;
  public veiculoCadastrado = false;
  public mensal = false;
  public descPlano = 'Avulso'

  constructor(public util: UtilService, public svc: GeralService,  public modalService: NgbModal, private cookie : CookieService) { }

  ngOnInit() {
    this.newEntrance();
  }

  newEntrance(){
    this.dadosVeiculo = {
      model : "",
      color : "",
      plate : ""
    };        
    this.descPlano = 'Avulso'
    this.dadosOK = false;
    this.preencherManual = false;
    this.parkingLot = undefined;
    this.lista = [];
    this.placa = '';
    this.listaEfetiva = [];
    this.id_user = null;
    this.veiculoCadastrado = false;
    this.mensal = false;
  }

  validaMensal(){
    this.svc.getListUsers().subscribe(res=>{      
      let users = JSON.parse((<any>res)._body);
      users.forEach(user => {        
        if (user.id == this.id_user){                    
          if (user.role == 3){            
            this.mensal = true;
            this.util.stopLoad();
            return false;            
          } else            
            this.mensal = false;          
        }                                 
      });
      this.util.stopLoad();
    console.log(this.mensal)})
  }

  vinculaUserVeic(){
    this.svc.doRegister({email:this.placa+'@default.com',name:this.placa, password:this.placa}).subscribe(res=>{                    
      this.id_user=JSON.parse((<any>res)._body).id;
      this.svc.updateVeiculo({plate:this.placa,id_user:this.id_user}).subscribe(res=>{
        this.util.stopLoad();
      });    
    });
  }
    
  getDadosPlaca(){   
    if (!this.testaPlaca(this.placa)){
      this.util.msgError('Ops!','Placa no formato incorreto!');
      return false;
    }
    this.placa = this.placa.toUpperCase();
    this.util.startLoad();
    
    this.svc.getVeiculo(this.placa).subscribe(res=>{      
      let veic = JSON.parse((<any>res)._body);               
      this.id_user = veic[0].id_user;
      if (veic[0].id_user == null){
        this.svc.doRegister({email:this.placa+'@default.com',name:this.placa, password:this.placa}).subscribe(res=>{                    
          this.id_user=JSON.parse((<any>res)._body).id;
          this.svc.updateVeiculo({plate:this.placa,id_user:this.id_user}).subscribe(res=>{
            this.validaMensal();            
          });
        })  
      } else 
        this.validaMensal();        
      
      this.dadosVeiculo = veic[0];
      this.dadosOK = true;            
    },err=>{            
      this.svc.getPlacaSinesp(this.placa).subscribe(res=>{    
        let veic;
        try {
          veic = JSON.parse((<any>res)._body);     
          if (!veic.modelo){
            throw "";            
          }
          this.dadosVeiculo.model = veic.modelo;
          this.dadosVeiculo.color = veic.cor;
          this.dadosVeiculo.plate = this.placa;              
          this.svc.addVeiculo(this.dadosVeiculo).subscribe(res=>{                    
            this.vinculaUserVeic();
            this.dadosOK = true;
          }, err=>{                  
            this.util.stopLoad();
            this.util.msgError('Ops!','Erro ao cadastrar o veículo, contate o administrador do sistema!');          
          });          
        } catch (error) {
          this.util.stopLoad();
          this.preencherManual = true;
          this.dadosOK = true;            
          this.util.msgInfo('Ops!','Não foi possível consultar os dados do veículo na SINESP! Informe os dados do veículo manualmente.');                              
        }        
      },err=>{
        this.util.stopLoad();
        this.preencherManual = true;
        this.dadosOK = true;
        this.util.msgInfo('Ops!','Não foi possível consultar os dados do veículo na SINESP! Informe os dados do veículo manualmente.');          
        }
      );
    });
  }
  
  realizarEntrada(content){ 
    this.util.startLoad();   
    if (this.preencherManual){
      this.dadosVeiculo.plate = this.placa;
      this.svc.addVeiculo(this.dadosVeiculo).subscribe(res=>{              
        this.svc.doRegister({email:this.placa+'@default.com',name:this.placa, password:this.placa}).subscribe(res=>{                    
          this.id_user=JSON.parse((<any>res)._body).id;
          this.svc.updateVeiculo({plate:this.placa,id_user:this.id_user}).subscribe(res=>{
            if (this.descPlano == 'Mensal'){
              this.svc.doPaymentMonth({id_user:this.id_user}).subscribe(res=>{
                this.svc.novoToken().subscribe(res =>{
                  let retorno = JSON.parse((<any>res)._body);
                  this.cookie.set('token', retorno.access_token);
                  this.svc.token = retorno.access_token;
                  this.efetivaEntrada(content);
                });          
              })
            } else
            this.efetivaEntrada(content);
          });    
        });        
      });
    } else {
      if (this.descPlano == 'Mensal'){
        this.svc.doPaymentMonth({id_user:this.id_user}).subscribe(res=>{
          this.svc.novoToken().subscribe(res =>{
            let retorno = JSON.parse((<any>res)._body);
            this.cookie.set('token', retorno.access_token);
            this.svc.token = retorno.access_token;
            this.efetivaEntrada(content);
          });          
        })
      } else 
        this.efetivaEntrada(content);
      
    }        
  }  

  efetivaEntrada(content){
    this.svc.doEntradaEstacionamento(this.placa).subscribe(res=>{
      this.util.stopLoad();      
      this.listaServicos(JSON.parse((<any>res)._body),content);            
    },err=>{
      this.util.stopLoad();
      this.util.msgError('Erro!',JSON.stringify(err));
    })             
  }

  cancelarEntrada(){
    this.newEntrance();
  }

  listaServicos(parkingLot,content) {    
    this.parkingLot = parkingLot;
    this.util.startLoad();
    this.svc.getListServicos().subscribe(res=>{        
      this.lista = (JSON.parse((<any>res)._body));            
      this.util.stopLoad();        
      this.modalService.open(content).result.then((result) => {             
        }, (reason) => {      
      });                           
    },err=>{
      this.util.stopLoad();
      this.util.msgError("Erro!","Ocorreu um erro ao carregar os serviços adicionais.");
    })       
  }

  addServices(){
    
    this.listaEfetiva.forEach(service => {
      this.svc.doAddService({id_parking_lot:this.parkingLot.id,id_additional_service:service}).subscribe(res => {                  
      });    
    });  
    this.util.msgSuccess('Sucesso!','Entrada do veículo foi realizada!');    
    this.newEntrance();
  }

  changeCheckbox(evt,service){
    if (evt.target.checked){
      this.listaEfetiva.push(service.id);
    } else {
      if (this.listaEfetiva.indexOf(service.id) >= 0)
        this.listaEfetiva.splice(this.listaEfetiva.indexOf(service.id));
    }    
  }

  dataAtualFormatada(){    
    var data = new Date();
    var dia = data.getDate();
    if (dia.toString().length == 1)
      dia = (<any>"0"+dia);
    var mes = data.getMonth()+1;
    if (mes.toString().length == 1)
      mes = (<any>"0"+mes);
    var ano = data.getFullYear();  
    var hora = data.getHours();
    var min = data.getMinutes();

    let minStr : string = min.toString();

    if (minStr.length == 1)
      minStr = '0'+minStr;
      

    return dia+"/"+mes+"/"+ano+' - '+hora+':'+minStr;  
  }

  testaPlaca(placa) {
    let padrao = /^[a-zA-Z]{3}\d{4}$/
    let padrao2 = /^[a-zA-Z]{3}-\d{4}$/
    let OK = padrao.exec(placa);
    let OK2 = padrao2.exec(placa);
        
    if (OK || OK2)
      return true
     else
      return false      
    
  }

}

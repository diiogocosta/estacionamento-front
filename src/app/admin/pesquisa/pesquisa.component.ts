import { UtilService } from './../../services/util.service';
import { Component, OnInit } from '@angular/core';
import { DatePipe } from '@angular/common';
import { GeralService } from './../../services/geral.service';
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
import { forEach } from '@angular/router/src/utils/collection';

@Component({
  selector: 'app-pesquisa',
  templateUrl: './pesquisa.component.html',
  styleUrls: ['./pesquisa.component.scss']
})
export class PesquisaComponent implements OnInit {
  public anonParkingLot;
  public listaVeiculos;  
  public parkingLot_id;
  public dadosVeiculo;
  public dadosOK = false;
  public price : any;
  public placa = '';
  settings = {
    mode : 'external',
    actions : {
      columnTitle: 'Detalhes',
      position : 'right',
      add : false,      
      edit : true,
      delete : false,      
    },
    edit : {
      editButtonContent : "Ver"      
    },
    columns: {
      plate: {
        title: 'Placa',        
      },
      model: {
        title: 'Modelo'
      },
      color: {
        title: 'Cor'
      },
      dt_entrance: {
        title: 'Data de Entrada'
      },              
      dt_exit: {
        title: 'Data de Saída'
      },
      released : {
        title: 'Status',
        type: 'string',          
          filter: {
            type: 'list',          
            config: {
              selectText: 'Todos',
              list: [
                { value: 'Não Liberado', title: 'Não Liberado' },
                { value: 'Liberado.', title: 'Liberado' },                
              ],
            },
          }
        },                       
    }   
  };

  constructor(
    public util : UtilService, 
    public svc : GeralService,
    public datePipe:DatePipe,
    public modalService: NgbModal
  ) { }

  ngOnInit() {    
    if (this.svc.role == 1 || this.svc.role == 2) 
      this.refreshPatio()  
  }

  refreshPatio(){
    this.util.startLoad();
    this.svc.patioListAll(' ').subscribe(res=>{
      this.util.stopLoad();
      this.listaVeiculos = JSON.parse((<any>res)._body);      
      let i;
      for (i = 0; this.listaVeiculos.length;i++){
        this.listaVeiculos[i].dt_entrance = this.datePipe.transform(this.listaVeiculos[i].dt_entrance, 'dd/MM/yyyy HH:mm')
        this.listaVeiculos[i].dt_exit = this.datePipe.transform(this.listaVeiculos[i].dt_exit, 'dd/MM/yyyy HH:mm')
         
        if (this.listaVeiculos[i].dt_exit == null)
        this.listaVeiculos[i].dt_exit = '-';
        
        if (this.listaVeiculos[i].released == 1)
          this.listaVeiculos[i].released = 'Liberado.'
        else
          this.listaVeiculos[i].released = 'Não Liberado'
      }      
    },err=>{
      this.util.stopLoad();
      this.util.msgError("Erro!","Não foi possível atualizar a lista de veículos no pátio.")      
    });
  }

  openDetail(parkingLot,content){    
    this.parkingLot_id = parkingLot.data.id;
    console.log(parkingLot.data);
    this.util.startLoad();
    this.svc.getPrice(parkingLot.data.id).subscribe(res=>{
    this.price = JSON.parse((<any>res)._body);
    console.log(this.price);
    console.log(parkingLot.data);
    this.price.title = 'A receber';
      if (this.price.monthly_user)
        this.price.userType = 'Mensalista'
      else if (this.price.hourly_user)
        this.price.userType = 'Horista'
      else
        this.price.userType = 'Administrador/funcionário'    
        
      
      if (parkingLot.data.paid){        
        this.price.parkingLotPrice = parkingLot.data.paid.parkingLotPaid;
        this.price.totalAdditionalServices = parkingLot.data.paid.totalAdditionalServices;
        this.price.totalParkingLot = parkingLot.data.paid.totalParkingLot;   
        this.price.additionalServices = parkingLot.data.paid.additionalServices;
        this.price.title = 'Pago';             
      }            
      
      if (this.price.additionalServices.length > 0){
        this.svc.getListServicos().subscribe(res=>{
          this.price.descriptionServices = [];
          let servicos = JSON.parse((<any>res)._body);
          servicos.forEach(servico => {
            this.price.additionalServices.forEach(addService => {
              if (addService.id_additional_service == servico.id)              
                this.price.descriptionServices.push({price:addService.price,description:servico.description});                              
            });            
          });
        })
      }

      

       this.util.stopLoad();
       this.modalService.open(content).result.then((result) => {             
          }, (reason) => {      
        });                           
     })        
  }

  consultaAnonima(){
    this.placa = this.placa.toUpperCase();
    this.util.startLoad();
    this.svc.getVeiculo(this.placa).subscribe(res=>{      
      let veic = JSON.parse((<any>res)._body);            
      this.dadosVeiculo = veic[0];
      this.dadosOK = true;      
      this.svc.listVeiculoParkingLot(this.placa).subscribe(res=>{
        let retorno = JSON.parse((<any>res)._body);
        let ultimoPkgLot = 0;
  
        retorno.forEach(lot => {
          if (lot.id > ultimoPkgLot)
          {
            ultimoPkgLot = lot.id;
            this.anonParkingLot = lot;
          }                
        });      
        this.svc.getPrice(this.anonParkingLot.id).subscribe(res=>{
          this.price = JSON.parse((<any>res)._body);
            if (this.price.monthly_user)
              this.price.userType = 'Mensalista'
            else if (this.price.hourly_user)
              this.price.userType = 'Horista'
            else
              this.price.userType = 'Administrador/funcionário'    
             this.util.stopLoad();           
           })                    
      },err=>{
        this.util.stopLoad();
        this.util.msgInfo('Informação!','Veículo não encontrado!');
      })
    },err=>{
      this.util.stopLoad();
      this.util.msgInfo('Informação!','Veículo não encontrado!');      
    });    
  }

  clear(){
    this.anonParkingLot = undefined;    
    this.dadosOK = false;
    this.placa = '';
  }
  

}

import { GeralService } from './../services/geral.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.scss']
})
export class AdminComponent implements OnInit {
  public backgroundImage = './assets/imgs/bg_cars.png';

  constructor(public svc: GeralService) { }

  ngOnInit() {
  }

  logout(){
    this.svc.doLogout();
  }

}

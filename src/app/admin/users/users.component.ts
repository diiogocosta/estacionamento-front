import { Component, OnInit } from '@angular/core';
import { UtilService } from '../../services/util.service';
import { GeralService } from '../../services/geral.service';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit {
  public currentUser;    
  public inputType = 'password';
  public rdyOnly = false;
  public users = [];

  constructor(private util:UtilService, private svc : GeralService) { }

  ngOnInit() {
    this.clear()
    this.list();
  }

  clear(){
    this.currentUser = {
      id : null,
      email : null,
      name : null,
      password : null,
      role : null,
      descRole : 'Funcionário'
    }
    this.rdyOnly = false;
    this.users = [];
  }

  list(){
    this.util.startLoad()
    this.svc.getListUsers().subscribe(res=>{
      let allUsers = JSON.parse((<any>res)._body);
      console.log(allUsers);

      allUsers.forEach(user => {
        if (user.role == 1 || user.role ==2){
          console.log(user.role);
          if (user.role == 1){
            user.descRole = 'Administrador'
          } else {
            user.descRole = 'Funcionário';
          }

          this.users.push(user);          
        }
      });
      console.log(this.users);
      this.util.stopLoad();
    })
  }

  goDetail(user){
    this.currentUser = user;
    this.rdyOnly = true;
  }

  register(){          
    this.util.startLoad();     
    if (this.currentUser['descRole'] == 'Administrador')
      this.currentUser['role'] = 1
    else  
      this.currentUser['role'] = 2;
    
    console.log(this.currentUser);

    if (this.currentUser['id'] == null){      
      this.svc.doRegister(this.currentUser).subscribe(res=>{        
        this.util.stopLoad();
        this.util.msgSuccess('Sucesso!','Funcionário gravado!');        
        this.clear();   
        this.list();
      }, err=>{        
        let errMsg = JSON.parse((<any>err)._body);
        this.util.stopLoad();
        if (errMsg.password)
          this.util.msgError('Ops.','Senha precisa ter pelo menos 6 dígitos.');
        if (errMsg.name)
          this.util.msgError('Ops.','Informe o nome!');           
        if (errMsg.email)
          this.util.msgError('Ops.','Email já está sendo utilizado!');                     
      })      
    } else {
      this.svc.doUpdateUser(this.currentUser).subscribe(res=>{                
        this.util.stopLoad();
        this.util.msgSuccess('Sucesso!','Funcionário atualizado!');   
        this.clear();        
        this.list();
      }, err=>{        
        let errMsg = JSON.parse((<any>err)._body);
        this.util.stopLoad();
        if (errMsg.password)
          this.util.msgError('Ops.','Senha precisa ter pelo menos 6 dígitos.');
        if (errMsg.name)
          this.util.msgError('Ops.','Informe o nome!');           
        if (errMsg.email)
          this.util.msgError('Ops.','Email já está sendo utilizado!');                     
      })        
    }      
  }
}
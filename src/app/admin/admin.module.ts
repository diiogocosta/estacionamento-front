import { PesquisaComponent } from './pesquisa/pesquisa.component';
import { EntradaVeiculoComponent } from './entrada-veiculo/entrada-veiculo.component';
import { AdminComponent } from './admin.component';
import { NgModule } from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';
import { AdminRoutes } from './admin-router';
import { FormsModule } from '@angular/forms';
import { PatioComponent } from './patio/patio.component';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { ValoresComponent } from './valores/valores.component';
import { RelatoriosComponent } from './relatorios/relatorios.component';
import { UsersComponent } from './users/users.component';

@NgModule({
  imports: [
    CommonModule,        
    FormsModule,
    Ng2SmartTableModule,    
    AdminRoutes
  ],
  declarations: [AdminComponent,EntradaVeiculoComponent, PatioComponent, ValoresComponent, PesquisaComponent, RelatoriosComponent, UsersComponent],
  providers:[DatePipe]
})
export class AdminModule { }

import { DatePipe } from '@angular/common';
import { UtilService } from './../../services/util.service';
import { GeralService } from './../../services/geral.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-valores',
  templateUrl: './valores.component.html',
  styleUrls: ['./valores.component.scss']
})
export class ValoresComponent implements OnInit {  
  public mensal = {
    id : "",
    price : "",
    updated_at : ""
  }
  public avulso = {
    id : "",
    price : "",
    updated_at : ""
  }
  public lista;
  public cadastro = {
    id : "",
    description : "",
    price : ""
  }

  constructor(public svc: GeralService,
    public util: UtilService,
    public datePipe: DatePipe) { }

  ngOnInit() {
    this.getList();
  }

  getList(){
    this.util.startLoad();
    this.svc.getPrecos().subscribe(res=>{            
      this.util.stopLoad();
      let precos = JSON.parse((<any>res)._body);      
      console.log(precos);
      precos.forEach(preco => {
        if (preco.name == 'Taxa mensal'){          
          this.mensal.id = preco.id;
          this.mensal.price = preco.price;
          this.mensal.updated_at = this.datePipe.transform(preco.updated_at, 'dd/MM/yyyy HH:mm');
        } else {
          this.avulso.id = preco.id;
          this.avulso.price = preco.price;
          this.avulso.updated_at = this.datePipe.transform(preco.updated_at, 'dd/MM/yyyy HH:mm');
        }      
      });
    },err=>{
      this.util.stopLoad();
    })

    this.util.startLoad();
    this.svc.getListServicos().subscribe(res=>{
      this.lista = (JSON.parse((<any>res)._body));      
      this.util.stopLoad();
    }, err => {      
      this.util.msgError('Ops!','Ocorreu um erro ao carregar os serviços adicionais.');
      this.util.stopLoad();
    })
  }

  goDetail(item){    
    this.cadastro.id = item.id;
    this.cadastro.description = item.description;
    this.cadastro.price = item.price;    
  }

  salvar(preco){
    this.util.startLoad();
    this.svc.doUpdatePrecos(preco).subscribe(res=>{
      this.util.stopLoad();
      this.util.msgSuccess('Sucesso!','O preço foi atualizado!');
      this.getList();
    },err=>{
      this.util.msgError('Ops!','Ocorreu um erro na atualização de preços.')
      this.util.stopLoad();
    })
  }

  salvarServico(item){    
    if (item.id != ''){
      this.util.startLoad();
      this.svc.doUpdateServicos(item).subscribe(res=>{
        this.util.stopLoad();        
        this.getList();      
        this.util.msgSuccess('Sucesso!',"Serviço adicional gravado!");        
      }, err =>{
        this.util.msgError('Ops!',"Ocorreu um erro ao gravar o serviço adicional.")        
        this.util.stopLoad();
      })
    } else {
      this.util.startLoad();
      this.svc.doAddServicos(item).subscribe(res=>{
        this.util.stopLoad();        
        this.getList();      
        this.util.msgSuccess('Sucesso!',"Serviço adicional gravado!");          
      }, err =>{
        this.util.msgError('Ops!',"Ocorreu um erro ao gravar o serviço adicional.")        
        this.util.stopLoad();
      });
    }
  }

  excluir(item){
    this.util.startLoad();
    this.svc.doDeleteServicos(item).subscribe(res=>{
      this.util.stopLoad();
      this.getList();      
      this.util.msgSuccess('Sucesso!','Serviço adicional excluído.')
    },err=>{
      this.util.stopLoad();
      this.util.msgError('Ops!','Ocorreu um erro ao excluir o serviço adicional.')
    })
  }

  clear(){        
    this.cadastro.description = "";
    this.cadastro.price = "";
    this.cadastro.id = "";
  }

}

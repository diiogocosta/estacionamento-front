import { GeralService } from './../../services/geral.service';
import { Component, OnInit } from '@angular/core';
import { UtilService } from '../../services/util.service';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-relatorios',
  templateUrl: './relatorios.component.html',
  styleUrls: ['./relatorios.component.scss']
})
export class RelatoriosComponent implements OnInit {
  private baseInfoPeriodo;
  private baseInfoDay;
  public infoDia = {entradas:null, pagos:null, adicionais:null, total:null};
  public infoPeriodo = {entradas:null, pagos:null, adicionais:null, total:null};
  public selectPeriodIni;  
  public selectPeriodFim;   

  constructor(private util : UtilService, private svc : GeralService, private datePipe : DatePipe) {    
   }

  ngOnInit() {    
    this.refreshPatio()  
  }

  onSelect(evt){

  }

  clear(){    
    this.infoPeriodo = {entradas:null, pagos:null, adicionais:null, total:null};
  }

    refreshPatio(){    
    this.svc.patioListAll('?exited=true').subscribe(res=>{      
      this.baseInfoPeriodo = JSON.parse((<any>res)._body);         
      for (let i = 0; i<this.baseInfoPeriodo.length;i++){        
        this.baseInfoPeriodo[i].dt_exit = this.datePipe.transform(this.baseInfoPeriodo[i].dt_exit, 'yyyy-MM-dd')                
        
        let dataAtual = new Date();        
        let dataAtualStr = dataAtual.toDateString();
        dataAtualStr = this.datePipe.transform(dataAtualStr, 'yyyy-MM-dd');
        dataAtual = new Date(dataAtualStr);        

        let todayPark = new Date(this.baseInfoPeriodo[i].dt_exit);        
        if (dataAtual.getTime() == todayPark.getTime()){          
          this.infoDia.entradas = this.infoDia.entradas+1;
          if (this.baseInfoPeriodo[i].paid){
            this.infoDia.pagos = this.infoDia.pagos+this.baseInfoPeriodo[i].paid.parkingLotPaid;
            this.infoDia.adicionais = this.infoDia.adicionais+this.baseInfoPeriodo[i].paid.totalAdditionalServices;
            this.infoDia.total = this.infoDia.total + this.baseInfoPeriodo[i].paid.totalParkingLot;        
          }        
        }
      }

    },err=>{      
      this.util.msgError("Erro!","Não foi possível carregar informações transacionais.")      
    });
  }  

  changePeriodo(){      
    this.clear();   

    if (!this.selectPeriodIni && !this.selectPeriodFim){
      this.util.msgInfo('Informação!','Período Inválido.')
      return false;
    }
      
    let dataIni = new Date(this.selectPeriodIni);
    let dataFim = new Date(this.selectPeriodFim);


    this.baseInfoPeriodo.forEach(info => {      
      let date = new Date(info.dt_exit);             
     
      console.log(info);
      if (date >= dataIni && date <= dataFim){                
        this.infoPeriodo.entradas = this.infoPeriodo.entradas+1;        
        if (info.paid){
          this.infoPeriodo.pagos = this.infoPeriodo.pagos+ info.paid.parkingLotPaid;        
          this.infoPeriodo.adicionais = this.infoPeriodo.adicionais+info.paid.totalAdditionalServices;        
          this.infoPeriodo.total = this.infoPeriodo.total+info.paid.totalParkingLot;              
        }
      }                  
    });
  }

}

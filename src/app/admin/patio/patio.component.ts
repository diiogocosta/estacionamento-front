import { GeralService } from './../../services/geral.service';
import { Component, OnInit } from '@angular/core';
import { UtilService } from '../../services/util.service';
import { DatePipe } from '@angular/common';
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-patio',
  templateUrl: './patio.component.html',
  styleUrls: ['./patio.component.scss']
})
export class PatioComponent implements OnInit {
  public listaVeiculos;
  public liberado = false;
  public parkingLot_id;
  public price : any;
  settings = {
    mode : 'external',
    actions : {
      columnTitle: 'Detalhes',
      position : 'right',
      add : false,      
      edit : true,
      delete : false,      
    },
    edit : {
      editButtonContent : "Ver"      
    },
    columns: {
      plate: {
        title: 'Placa',         
      },
      model: {
        title: 'Modelo'
      },
      color: {
        title: 'Cor'
      },
      dt_entrance: {
        title: 'Data de Entrada'
      },              
    }   
  };

  constructor(public svc:GeralService, 
    public util:UtilService, 
    public datePipe: DatePipe,
    public modalService: NgbModal) { }

  ngOnInit() {
    this.refreshPatio();
  }

  refreshPatio(){
    this.util.startLoad();
    this.svc.patioListAll().subscribe(res=>{
      this.util.stopLoad();
      this.listaVeiculos = JSON.parse((<any>res)._body);      
      let i;
      for (i = 0; this.listaVeiculos.length;i++){
        this.listaVeiculos[i].dt_entrance = this.datePipe.transform(this.listaVeiculos[i].dt_entrance, 'dd/MM/yyyy HH:mm')
      }      
    },err=>{
      this.util.stopLoad();
      this.util.msgError("Erro!","Não foi possível atualizar a lista de veículos no pátio.")      
    });
  }

  openDetail(parkingLot,content){
    this.liberado = false;
    this.parkingLot_id = parkingLot.data.id;
    this.util.startLoad();
    this.svc.getPrice(parkingLot.data.id).subscribe(res=>{
    this.price = JSON.parse((<any>res)._body);
    console.log(this.price);
      if (this.price.monthly_user)
        this.price.userType = 'Mensalista'
      else if (this.price.hourly_user)
        this.price.userType = 'Horista'
      else
        this.price.userType = 'Administrador/funcionário'

      if (this.price.additionalServices.length > 0){
        this.svc.getListServicos().subscribe(res=>{
          this.price.descriptionServices = [];
          let servicos = JSON.parse((<any>res)._body);
          servicos.forEach(servico => {
            this.price.additionalServices.forEach(addService => {
              if (addService.id_additional_service == servico.id)
                this.price.descriptionServices.push({price:addService.price,description:servico.description});                                              
            });            
          });
        })
      }        

       this.util.stopLoad();
       this.modalService.open(content).result.then((result) => {             
          }, (reason) => {      
        });                           
     })        
  }

  realizarSaida(price){
    this.util.startLoad();    
    this.svc.doPaymentAdditionalServices({id_parking_lot:this.parkingLot_id}).subscribe(res=>{    
      if (!price.monthly_user){
        this.svc.doPaymentHourly({id_parking_lot:this.parkingLot_id}).subscribe(res=>{
        },err=>{this.util.msgError('Avisar Fornecedor Software!',JSON.stringify(res))})
      }
      this.svc.doLiberaVeiculo({id:this.parkingLot_id}).subscribe(res=>{
        this.svc.doExitVeiculo({id:this.parkingLot_id}).subscribe(res=>{
          this.util.stopLoad();
          this.liberado = true;
          this.refreshPatio();
          this.util.msgSuccess('Informação!','Veículo liberado para saída.');                  
        },err=>{
          this.util.stopLoad();
          this.util.msgError('Erro!','Erro ao dar a saída do veículo.');                
        })
      },err=>{      
        this.util.stopLoad();
        this.util.msgError('Erro!','Erro ao liberar o veículo.');      
      })
    })
  }

}

import { RelatoriosComponent } from './relatorios/relatorios.component';
import { ValoresComponent } from './valores/valores.component';
import { AuthGuard } from './../guards/auth-guard';
import { AdminComponent } from './admin.component';
import { EntradaVeiculoComponent } from './entrada-veiculo/entrada-veiculo.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PatioComponent } from './patio/patio.component';
import { PesquisaComponent } from './pesquisa/pesquisa.component';
import { UsersComponent } from './users/users.component';
// canActivate: [AuthGuard]
const routes: Routes = [
    { path: '', component: AdminComponent,
      children:[{
        path: 'entradas',
        component: EntradaVeiculoComponent,
        canActivate: [AuthGuard]
      },
      {
        path: 'patio',
        component: PatioComponent,
        canActivate: [AuthGuard]
      },
      {
        path: 'pesquisa',
        component: PesquisaComponent,
        canActivate: [AuthGuard]
      },
      {
        path: 'valores',
        component: ValoresComponent,
        canActivate: [AuthGuard]
      },
      {
        path: 'relatorios',
        component: RelatoriosComponent,
        canActivate: [AuthGuard]
      },
      {
        path: 'funcionarios',
        component: UsersComponent,
        canActivate: [AuthGuard]
      }      
    ]},             
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class AdminRoutes {}

